import pdfkit
import click

# sudo apt-get install wkhtmltopdf


@click.command()
@click.option("--orientation",
               default="portrait", help="Orientation of the pdf file")
def generate(orientation):
    try:
        options = {
            "page-size": "Legal",
            "orientation": orientation,
            "enable-local-file-access":None
        }
        pdfkit.from_file("salida.html", "salida_kit.pdf", options=options)
    except Exception as e:
        raise e

if __name__ == "__main__":
    generate()
