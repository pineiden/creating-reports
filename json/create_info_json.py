import json
from pathlib import Path
from ..param_trigonometricos import data as data_trig
from ..param_nivel_transversal import data as data_niv_tr
from ..param_nivel_campana import data as data_niv_cp
from ..param_nivel_longitudinal import data as data_niv_ln


data_dict = {
        "trigonometrico": data_trig,
        "nivel_transversal": data_niv_tr,
        "nivel_campana": data_niv_cp,
        "nivel_longitudinal": data_niv_ln,
}


def run(template_type, output_file, dir_img, images_fields):
    
    data = data_dict.get(template_type)
    path_img = Path(dir_img)
    
    for (field, sub_field), filename in images_fields:
        if sub_field in data[field]:
            data[field][sub_field]["image"] = path_img / filename

    if data is not None:
        print("Saving json...")
        with open(output_file, "w") as f:
            json.dump(data, f, indent=2, default=str)
