from datetime import datetime
from pathlib import Path
from jinja2 import Environment, FileSystemLoader, select_autoescape
from pprint import pprint
import click
from rich import print
from param_trigonometricos import data as data_trigo
from param_nivel_transversal import data as data_nivel_transversal
from param_nivel_longitudinal import data as data_nivel_longitudinal
from param_nivel_campana import data as data_nivel_campana
from pathlib import Path


this = Path(__file__).parent.absolute()
env = Environment(
    loader=FileSystemLoader(this / "templates"),
    autoescape=select_autoescape()
)


"""
This can be an object depends of the framework
for every css id five a list of css class to put
"""


def load_template(template_path, data):
    try:
        print(template_path, Path(template_path).exists())
        template = env.get_template(template_path)
        result = template.render(**data)
        return result
    except Exception as e:
        print(e)
        raise e


def save_template(html, name="salida.html"):
    print("Saving new template html...")
    if html:
        with open(name, "w") as f:
            f.write(html)
            print(f"Saved file {name}")

"""
Enable livereload server
"""


@click.command()
@click.option("--template_type",
               default="trigonometrico", help="A type of template: trigonometrico, nivel or nivel_transversal_")
def reload_template(template_type):
    print(f"Reloading templat {template_type}")
    dataset = {
        "trigonometrico": data_trigo,
        "nivel_transversal": data_nivel_transversal,
        "nivel_campana": data_nivel_campana,
        "nivel_longitudinal": data_nivel_longitudinal
    }
    templates = {
        "trigonometrico": "template.jinja.html",
        "nivel_transversal": "template_nivelacion_transversal.jinja.html",
        "nivel_campana": "template_nivelacion_campana.jinja.html",
        "nivel_longitudinal": "template_nivelacion_longitudinal.jinja.html",
    }
    template_type = template_type.lower()
    print(f"Reloading template {template_type}")
    if template_type in templates:
        template_html = templates.get(template_type)
        data = dataset.get(template_type)
        result = load_template(str(template_html), data)
        save_template(result)
        print("new salida.html done")
    else:
        print(f"the template {template_type} doesn't exists")



if __name__ == '__main__':
    reload_template()
