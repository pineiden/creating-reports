from datetime import datetime
from pathlib import Path
"""
Create file, loading template and data
"""
this = Path(__file__).parent

css = {
    "bulma":  "./styles/bulma/css/bulma.min.css",
    "especial":  "./styles/especial_nivel_longitudinal.css"
}


style_classes = {
    "body": " ".join(["container", "is-fullhd"]),
    "main": " ".join(["has-text-centered", "is-three-fifths"]),
    "logos": " ".join(["columns", "is-mobile"]),
    "info": " ".join(["container", "margin-bottom"]),
    "placa": " ".join(["has-text-centered"]),
    "extra": " ".join(["column"]),
    "monografia": " ".join(["column is-half"]),
    "notes": " ".join(["column"]),
    "graficos": " ".join(["column", "graficos", "has-text-centered", "is-three-quarters"]),
    "notes_box_class": " ".join(["has-text-centered", "is-one-quarter"])
}


data = {
    "info": {
        "placa": {
            "id": 100,
            "project_title": {
                "text":"Metro Línea 7",
                "class": "report-title"},
            "title": {
                "text": "Monitoreo de deformaciones para las excavaciones de piques, galerías y túneles",
                "class": "report-title"
            },
            "tipo_eje": "Galería",
            "date": {
                "class": " ".join(["title", "is-4"]),
                "text": datetime.utcnow().date().isoformat()},
            "code": {
                "class": " ".join(["title", "is-5"]),
                "eje": "02",
                "seccion": "0007"
            },
            "cliente": "Metro",
            "contratista": "Geosinergia",
            "pk": {"min": "3+200", "max": "3+400"},
            "referencia": "Salvador Gutierrez",
            "fecha": datetime.now().date(),
            "class": " ".join(["table",
                               "is-bordered",
                               "is-striped",
                               "is-fullwidth", "has-text-centered"])
        },
    },
    "logos": {
        "contratista": {
            "image": Path("./img/geosinergia.svg"),
            "styles": {
                "id": "contratista_logo",
                "class": " ".join(["column", "has-text-right", "is-one-fifth"]),
                "class_in": " ".join([])
            }
        },
        "cliente": {
            "image": Path("./img/metro.svg"),
            "styles": {
                "id": "cliente_logo",
                "class": " ".join(["column", "has-text-left", "is-one-fifth"]),
                "class_in": " ".join([])
            }
        },
    },
    "graficos": {
        "settlement_longitudinal": {
            "figsize": (15, 5.14),
            "title": {
                "text": "Asentamiento longitudinal",
                "styles": {
                    "class": "chart-title"
                }
            },
            "image": Path("./img/chart_settlement_longitudinal.png"),
            "styles": {
                "id": "img_nivelacion_longitudinal",
                "class": " ".join(["chart-img"])
                #"class": "chart-img"
            }
        },
        "excavation_progress": {
            "figsize": (15, 4.5),
            "title": {
                "text": "Secuencia de excavación",
                "styles": {
                    "class": "chart-title"
                }
            },
            "image": Path("./img/chart_avance_excavacion.svg"),
            "styles": {
                "id": "img_avance_excavacion",
                "class": "chart-img"
            }
        }
    },
    "table": {
        "class": "",
        "styles": {
            "id_in": "box_table"},
        "titles": ["Sección", "PK", "Fecha", "Def. (mm)"],
        "max_rows": 8,
        "rows": [
            ["010001", "3+100", "12/07/21", "12.46"],
            ["010002", "3+200", "10/07/21", "11.32"],
            ["010011", "3+400", "11/07/21", "10.55"],
            ["010013", "3+500", "12/07/21", "10.21"],
            ["", "", "", ""],
            ["", "", "", ""],
            ["", "", "", ""],
            ["", "", "", ""],
        ],
    },
    "extra": {
        "extra_box": {
        "styles": {
                "id_in": "extra-id",
                "class": "extra-class",
                "class_in": "",
            }
            },
        "spacer": {
            "styles": {
                "id_in": "spacer_id",
                "class": "spacer-block",
                "class_in": "",
            }
        },
        "mono_box": {
        "styles": {
                "id_in": "box_monografia",
                "class": "extra-box-monografia",
                "class_in": "",
            }
            },
        "monografia": {
            "image": Path("./img/monografia_v2.png"),
            "styles": {
                "id": "monografia_fig",
                "id_in": " ".join(["monografia_box"]),
                "class": "extra-info-block",
                "class_in": " ".join(["monografia-class", ]),
            },
            "title": {
                "text": "Ubicación",
                "styles": {"class": " ".join(["has-text-centered",
                                              "report-title"])}
            }
        },
        "signos_box": {
        "styles": {
                "id_in": "box_signos",
                "class": "signos-box-class",
                "class_in": "",
            }
            },
        "signos_vert": {
            
            "image": Path("./img/signos_vert.svg"),
            "styles": {
                "id": "signos_fig",
                "class": "signos_figure",
            },
            "title": {
                "text": "Convención de signos",
                "styles": {"class": " ".join(["has-text-centered",
                                              "report-title"])}
            }
        },
        "notes": {
            "styles": {
                "id_in": " ".join(["notes_box"]),
                "class": "notes_box_class  is-one-quarter",
                "class_in": " ".join(["notes"]),
                "ul_class": " ".join([])
            },
            "list": [
                "cambio referencia en punto 03",
                "el punto 09 pasa a punto fijo"
            ],
            "title": {
                "text": "Notas",
                "styles": {"class": " ".join(["has-text-centered",
                                              "report-title"])}
            }
        },
    },
    "styles": style_classes,
    "css": css,
}
