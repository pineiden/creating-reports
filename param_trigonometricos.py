from datetime import datetime
from pathlib import Path
"""
Create file, loading template and data
"""
this = Path(__file__).parent

style_classes = {
    "body": " ".join(["container", "is-fullhd"]),
    "main": " ".join(["has-text-centered", "is-three-fifths"]),
    "logos": " ".join(["columns", "is-mobile"]),
    "info": " ".join(["container", "margin-bottom"]),
    "placa": " ".join(["has-text-centered"]),
    "extra": " ".join(["column", ]),
    "monografia": " ".join(["column"]),
    "notes": " ".join(["column"]),
    "graficos": " ".join(["column", "graficos", "has-text-centered"])
}

template_html = this / "templates"/"template.html.jinja"

css = {
    "bulma":  "./styles/bulma/css/bulma.min.css",
    "especial":  "./styles/especial.css"
}

data = {
    "css": css,
    "info": {
        "placa": {
            "id": 100,
            "project_title": {
                "text":"Metro Línea 7",
                "class": "report-title"},
            "title": {
                "text": "Monitoreo de deformaciones para las excavaciones de piques, galerías y túneles",
                "class": "report-title"
            },
            "tipo_eje": "Pique",
            "date": {
                "class": " ".join(["title", "is-4"]),
                "text": datetime.utcnow().date().isoformat()},
            "code": {
                "class": " ".join(["title", "is-5"]),
                "eje": "01",
                "seccion": "0001"
            },
            "cliente": "Metro",
            "contratista": "Geosinergia",
            "pk": "2+200",
            "referencia": "Salvador Gutierrez",
            "fecha": datetime.now().date(),
            "class": " ".join(["table",
                               "is-bordered",
                               "is-striped",
                               "is-fullwidth",
                               "has-text-centered"])
        },
    },
    "extra": {
        "mono_box": {
        "styles": {
                "id_in": "box_monografia",
                "class": "extra-box-monografia",
                "class_in": "",
            }
            },
        "monografia": {
            
            "image": Path("./img/monografia_v2.png"),
            "styles": {
                "id": "monografia_fig",
                "id_in": "",
                "class": "",
                "class_in": "",
            },
            "title": {
                "text": "Ubicación",
                "styles": {"class": " ".join(["has-text-centered",
                                              "extra-info-title"])}
            }
        },
        "signos_box": {
        "styles": {
                "id_in": "box_signos",
                "class": "",
                "class_in": "",
            }
            },
        "signos": {
            
            "image": Path("./img/signos.svg"),
            "styles": {
                "id": "signos_fig",
                "class": "",
            },
            "title": {
                "text": "Convención de signos",
                "styles": {"class": " ".join(["has-text-centered",
                                              "extra-info-title"])}
            }
        },
        "notes": {
            "styles": {
                "id_in": " ".join(["notes_box"]),
                "class": "extra-info-block",
                "class_in": " ".join(["notes"]),
                "ul_class": " ".join([])
            },
            "list": [
                "cambio referencia en punto 03",
                "el punto 09 pasa a punto fijo"
            ],
            "title": {
                "text": "Notas",
                "styles": {"class": " ".join(["has-text-centered",
                                              "extra-info-title"])}
            }
        },
    },
    "logos": {
        "contratista": {
            "image": Path("./img/geosinergia.svg"),
            "styles": {
                "id": "cliente_logo",
                "class": " ".join(["column", "has-text-left"]),
                "class_in": " ".join([])
            }
        },
        "cliente": {
            "image": Path("./img/metro.svg"),
            "styles": {
                "id": "contratista_logo",
                "class": " ".join(["column", "has-text-right"]),
                "class_in": " ".join([])
            }
        },
    },
    "graficos": {
        "displacement": {
            "figsize": (15, 10.5),
            "title": {
                "text": "Gráficos de deformaciones",
                "styles": {"class": "chart-title"}
                },
            "image": Path("./img/chart_trigonometrico.svg"),
            "styles": {"id": "img_trigonometrico",
                       "class": "chart-img"}
                },
        "excavation_progress": {
            "figsize": (15, 4.5),
            "title": {
                "text": "Secuencia de excavación",
                "styles": {"class": "chart-title"}
                },
            "image": Path("./img/chart_avance_excavacion.svg"),
            "styles": {"class": "chart-img",
                       "id": "img_progress"}
        }
    },
    "styles": style_classes,
    "css": css,
}


print(data)

