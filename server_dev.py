from functools import partial
from pprint import pprint

import click
import formic
from livereload import Server, shell
from rich import print


def css_modified(name, template):
    print(f"CSS modificado {name}")
    command = f"python load_template.py --template_type {template}"
    shell(command)()

def py_modified(name, template):
    print(f"Python file modified {name}")
    command = f"python load_template.py --template_type {template}"
    shell(command)()

def template_modified(name, template):
    print(f"Template modified {name}")
    command = f"python load_template.py --template_type {template}"
    shell(command)()



@click.command()
@click.option("--template",
              default="trigonometrico",
              help="tipo de template")
@click.option("--orientation",
              default="portrait",
              help="orientación de la página")
@click.option("--template",
              default="trigonometrico",
              help="tipo de template")
def command(template, orientation):
    print(f"Watching {template}")
    shell(f"python load_template.py --template_type {template}")()
    server = Server()
    for filepath in formic.FileSet(
            include="param**.py",
            directory="./"):
        server.watch(filepath, partial(py_modified,filepath, template))

    for filepath in formic.FileSet(
            include="**.css",
            directory="./styles"):
        server.watch(filepath, partial(css_modified,filepath, template))
    for filepath in formic.FileSet(
            include="**.jinja.html",
            directory="./templates"):
        server.watch(filepath,
                     partial(template_modified, filepath, template))
    server.watch(
        "load_template.py",
        shell(
            f"python load_template.py --template_type {template}")
    )
    server.watch(
        "salida.html",
        shell(
            f"python generate_pdf.py --orientation {orientation}")
    )
    server.serve()


if __name__ == '__main__':
    command()
